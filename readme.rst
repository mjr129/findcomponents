===============
Find components
===============

Lists connected components in a graph.

-----
Usage
-----

* Input is read from ``stdin``.
    * 2 column TSV should be presented, with both the columns denoting node IDs. Each line denotes an edge.
* All output is written to ``stdout``.
    * 2 column TSV is output, with the first column denoting node IDs and the second column denoting component IDs. Each line denotes an assignment of a node to a component.
* All progress messages are written to ``stderr``.

-------
Example
-------

::

    findcomponents < test.txt

-----------
Acquisition
-----------

The application is provided in source code format only.
Compile using cmake and c++14, no esoteric libraries are required.
