#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <unordered_set>
#include <stack>

std::map<int, std::string> name_lookup = std::map<int, std::string>();
std::map<int, std::vector<int>*> edges = std::map<int, std::vector<int>*>();


typedef std::map<std::string, int> TIdLookup;

void debug_print();
int get_id_from_name( const std::string& name, TIdLookup& id_lookup);
void load_file();


void create_edge( int left_id, int right_id );


/**
 * Entry point
 * @param argc 
 * @param argv 
 * @return 
 */
int main(int argc, const char** argv)
{
    load_file();

    std::unordered_set<int> open_list;
    
    for(int i = 0; i < name_lookup.size(); ++i)
    {
        open_list.insert(i);
    }
    
    auto components = std::vector<std::vector<int>*>();
    
    while (open_list.size())
    {
        auto component = new std::vector<int>();
        
        // Get a starting element
        auto it_next = open_list.begin();
        int first = *it_next;
        auto stack = std::stack<int>();
        stack.push(first);
        
        while (!stack.empty())
        {
            int next = stack.top();
            stack.pop();
            
            if (open_list.erase( next ) == 0)
            {
                // We have already dealt with this element
                continue;
            }
            
            component->push_back( next );

            auto it_friends = edges.find( next );

            if (it_friends != edges.end())
            {
                for (auto friend_ : *it_friends->second)
                {
                    stack.push(friend_);
                }
                
                edges.erase(it_friends);
            }
            
            
        }
        
        // Ignore length 1 components
        if (component->size() == 1)
        {
            delete component;
            continue;
        }
        
        std::cerr << "READ COMPONENT " << components.size() << " = " << component->size() << std::endl;
        components.push_back(component);
    }
    
    int component_index = 0;
    
    for (std::vector<int>* component : components)
    {
        std::cerr << "WRITE COMPONENT " << component_index << " = " << component->size() << std::endl;
        
        for(int element : *component)
        {
            std::cout << name_lookup.at(element) << "\tF" << component_index << std::endl;
        }
        
        delete component;
        
        component_index += 1;
    }
    
    // Clean up edges
    for (const auto& edge : edges)
    {
        delete edge.second;
    }

    return 0;
}



/**
 * Loads in the data (from STD-IN)
 */
void load_file()
{
    TIdLookup* id_lookup = new TIdLookup(); 
    
    std::__1::string cell;
    bool             left = true;

    int left_id = -1;

    while(getline( std::__1::cin, cell, left ? static_cast<char>('\t') : static_cast<char>('\n')))
    {
        if (left)
        {
            left_id = get_id_from_name(cell, *id_lookup);
        }
        else
        {
            int right_id = get_id_from_name(cell, *id_lookup);

            // This way is less memory efficient but should be faster
            create_edge( left_id, right_id );
            create_edge( right_id, left_id );

        }
        
        left = !left;
    }
    
    // debug_print();
    
    delete id_lookup;
}


void create_edge( int left_id, int right_id )
{
    auto it = edges.find( left_id);

    if (it == edges.end())
    {
        auto the_vector = new std::__1::vector<int>(); // NEW
        the_vector->push_back(right_id);
        edges[left_id] = the_vector;
    }
    else
    {
        edges[left_id]->push_back(right_id);
    }
}


/**
 * Prints out the edges and names  
 */
void debug_print()
{
    for (const auto & edge: edges)
    {
        for (const auto & right : *edge.second)
        {
            std::__1::cerr << "EDGE " << edge.first << " " << right << std::__1::endl;
        }
    }

    for (const auto& mapping : name_lookup)
    {
        std::__1::cerr << "NAME " << mapping.first << " " << mapping.second << std::__1::endl;
    }
}

/**
 * Finds or creates the ID for a name
 * @param name 
 * @return 
 */
int get_id_from_name( const std::string& name, TIdLookup& id_lookup)
{
    auto it = id_lookup.find(name);
    
    if (it == id_lookup.end())
    {
        int id = static_cast<int>(name_lookup.size());
        id_lookup[name] = id;
        name_lookup[id] = name;
        return id;
    }
    else
    {
        return it->second;
    }
}